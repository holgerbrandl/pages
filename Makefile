JEKYLL?=bundle exec jekyll
DST=_site
MAKEFILES=Makefile $(wildcard *.mk)

# Controls
.PHONY : print_help clean files
all : print_help

## prepare            : install dependencies locally
prepare :
	bundle install
	npm install gulp-cli # creates node_modules and puts binaries there

## images            : run gulp to create responsive clones of all images
images :
	./node_modules/gulp/bin/gulp.js img

## serve            : run a local server.
serve :
	${JEKYLL} serve

## site             : build files but do not run a server.
site :
	${JEKYLL} build

## clean            : clean up junk files.
clean :
	@rm -rf ${DST}
	@rm -rf .sass-cache
	@rm -rf bin/__pycache__
	@find . -name .DS_Store -exec rm {} \;
	@find . -name '*~' -exec rm {} \;
	@find . -name '*.pyc' -exec rm {} \;

## print_help       : show all print_help.
print_help :
	@grep -h -E '^##' ${MAKEFILES} | sed -e 's/## //g'
